# WebCrawler

This project implements a simple web crawler. 
It uses simple http requests and selenium for rendering javascript pages. 
It will parse href attributes from <a> tags and add those pages into a queue
which will be processed later. 

Requirements:
 * Docker CE
 * Python: ~3.6
 
Installation instructions:
 * RUN: docker-compose up -d in docker directory
 * Import crawldb.sql database structure into db docker image
 * Add seed pages into gov-sites.txt file
 * RUN: ./runInitialize.sh
 * RUN: ./runCrawler.sh #numberOfConsumers#
 
Where #numberOfConsumers" is the number of parallel consumers reading from 
the queue (eg .\runCrawler.sh 16 - this will start 16 parallel consumers reading from the queue).

When crawling a large scope of websites it is recommended you add the ./runCrawler.sh command
into crontab as this will insure that failed consumers are restarted periodically.

Example:
```
* * * * * cd /path/to/WebCrawler/ && ./runCrawler.sh 16
```


