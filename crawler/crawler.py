import datetime
import time
import json
import os
import re
import sys
import uuid
from urllib.parse import urlparse
from urllib.parse import urljoin
from urllib.request import urlopen
from urllib.error import HTTPError
import xml.etree.ElementTree as xmltree
import ssl

import pika
import psycopg2
import psycopg2.extras
import reppy.robots
import redis
import xxhash
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup

USER_AGENT = "MarkoBot<This is a FRI project. Contact: ma6795@student.uni-lj.si>"
MAX_LEVEL = 15
UNRENDERED_PAGE_CHARACTER_LIMITATION = 500
ALLOW_EXTERNAL_SITES = True


class Driver:
    def __init__(self, url):
        options = Options()
        options.add_argument("--headless")
        options.add_argument("start-maximized")
        options.add_argument("disable-infobars")
        options.add_argument("--disable-extensions")
        options.add_argument("--disable-gpu")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--no-sandbox")
        driver = webdriver.Chrome(executable_path=os.path.abspath('/usr/bin/chromedriver'), options=options)
        driver.implicitly_wait(5)
        driver.get(url)
        self.web_driver = driver

    def close(self):
        self.web_driver.close()


class Parser:
    @staticmethod
    def get_page_links(web_driver):
        return web_driver.find_elements_by_tag_name("a")

    @staticmethod
    def get_page_images(web_driver):
        return web_driver.find_elements_by_tag_name("img")

    @staticmethod
    def remove_html_tags(html):
        cleanr = re.compile('<.*?>')
        cleantext = re.sub(cleanr, '', html)
        return cleantext

    @staticmethod
    def validate_url(url):
        if not url:
            return False

        regex = re.compile(
            r'^(?:http|ftp)s?://' # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' # domain...
            r'localhost|' # localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|' # ...or ipv4
            r'\[?[A-F0-9]*:[A-F0-9:]+\]?)' # ...or ipv6
            r'(?::\d+)?' # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)

        if regex.match(url):
            return True
        return False

    @staticmethod
    def canonical_url(url, domain, include_www=True):
        if not url.startswith("http"):
            url = urljoin(domain, url)

        url_parts = urlparse(url, "http", False)

        canonical_url = url_parts.scheme+"://" + url_parts.netloc + url_parts.path
        #if len(url_parts.query) > 0:
        #    canonical_url += "?" + url_parts.query

        if len(url_parts.params) > 0:
            print("Url with params:", url)

        canonical_url = canonical_url.replace("#", "").replace("https", "http")

        if not include_www:
            canonical_url = canonical_url.replace("www.", "")

        return canonical_url

    @staticmethod
    def get_domain(url, strict=False):
        url_parts = urlparse(url, "http", False)
        if strict:
            return url_parts.netloc.strip("/").replace("www.", "")
        else:
            return url_parts.scheme+"://" + url_parts.netloc.strip("/")+"/"


class UrlFetcher:
    @staticmethod
    def getInitialUrls(fileName):
        pageUrls = []
        with open(fileName) as file:
            line = file.readline()
            while line:
                pageUrls.append(line.replace("\n", ""))
                line = file.readline()
        return pageUrls


class Database:
    connection = None
    cursor = None
    user = "root"
    password = "root"
    host = "10.1.0.2"
    database = "postgres"

    def connect(self, as_dict = False):
        self.connection = psycopg2.connect(
            user=self.user,
            password=self.password,
            host=self.host,
            database=self.database
        )

        if as_dict:
            self.cursor = self.connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
        else:
            self.cursor = self.connection.cursor()

    def insert_site(self, domain, robots_content, sitemap_content):
        self.cursor.execute(
            """INSERT INTO crawldb.site (domain, robots_content, sitemap_content) VALUES (%s, %s, %s)""",
            (domain, robots_content, sitemap_content)
        )
        self.connection.commit()
        self.cursor.execute(
            """SELECT id FROM crawldb.site WHERE domain = %s""",
            (domain,)
        )
        return self.cursor.fetchall()[0][0]

    def insert_page(self, site_id, page_type_code, url, html_content, http_status_code, accessed_time):
        self.cursor.execute(
            """INSERT INTO crawldb.page (site_id, page_type_code, url, html_content, http_status_code, accessed_time) VALUES (%s, %s, %s, %s, %s, %s)""",
            (site_id, page_type_code, url, html_content, http_status_code, accessed_time)
        )
        self.connection.commit()
        self.cursor.execute(
            """SELECT id FROM crawldb.page WHERE url = %s""",
            (url,)
        )
        return self.cursor.fetchall()[0][0]

    def update_page(self, url, site_id, page_type_code, html_content, http_status_code, accessed_time):
        self.cursor.execute(
            "UPDATE crawldb.page SET site_id = %s, page_type_code = %s, html_content = %s, http_status_code = %s, accessed_time = %s WHERE url LIKE %s",
            (site_id, page_type_code, html_content, http_status_code, accessed_time, url)
        )
        self.connection.commit()

    def insert_page_data(self, page_id, data_type_code, data):
        self.cursor.execute(
            """INSERT INTO crawldb.page_data (page_id, data_type_code, data) VALUES (%s, %s, %s)""",
            (page_id, data_type_code, data)
        )
        self.connection.commit()

    def insert_page_link(self, from_id, to_id):
        from_id = int(from_id)
        to_id = int(to_id)
        if from_id > 0 and to_id > 0:
            self.cursor.execute(
                """INSERT INTO crawldb.link (from_page, to_page) VALUES (%s, %s)""",
                (from_id, to_id)
            )
            self.connection.commit()

    def insert_image(self, page_id, filename, content_type, data):
        self.cursor.execute(
            """INSERT INTO crawldb.image (page_id, filename, content_type, data) VALUES (%s, %s, %s, %s)""",
            (page_id, filename, content_type, data)
        )
        self.connection.commit()

    def select_page(self, url):
        self.cursor.execute(
            """SELECT * FROM crawldb.page WHERE url LIKE %s""",
            (url,)
        )
        return self.cursor.fetchone()

    def select_website_domain(self, site_id):
        self.cursor.execute(
            """SELECT domain FROM crawldb.site WHERE id = %s""",
            (site_id,)
        )
        return self.cursor.fetchall()[0][0]

    def select_website(self, site_id):
        self.cursor.execute(
            """SELECT * FROM crawldb.site WHERE id = %s""",
            (site_id,)
        )
        return self.cursor.fetchone()

    def select_website_by_url(self, url):
        url = Parser.get_domain(url)

        self.cursor.execute(
            """SELECT * FROM crawldb.site WHERE domain = %s""",
            (url,)
        )

        result = self.cursor.fetchone()

        if result is None:
            url = Parser.canonical_url(url, url, include_www=False)
            self.cursor.execute(
                """SELECT * FROM crawldb.site WHERE domain = %s""",
                (url,)
            )
            result = self.cursor.fetchone()

        return result


class RabbitMq:
    host = '10.1.0.3'
    connection = None

    def connect(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(self.host))
        return self

    def get_channel(self):
        return self.connection.channel()

    def declare_queue(self, queue):
        self.get_channel().queue_declare(queue=queue)

    def enqueue(self, routing_key, body):
        self.get_channel().basic_publish(exchange='', routing_key=routing_key, body=body)


class Redis:
    host = '10.1.0.4'
    port = 6379
    db = 2
    connection = None

    def connect(self):
        self.connection = redis.Redis(host=self.host, port=self.port, db=self.db)

    def save_key(self, key):
        if key is not None:
            x = xxhash.xxh64()
            x.update(key)
            self.connection.set("url:"+x.hexdigest(), 1)

    def get_key(self, key):
        if key is not None:
            x = xxhash.xxh64()
            x.update(key)
            return self.connection.get("url:"+x.hexdigest())

    def save_content_key(self, key, page_id):
        if key is not None:
            x = xxhash.xxh64()
            x.update(key)
            self.connection.set("content:"+x.hexdigest(), page_id)

    def get_content_key(self, key):
        if key is not None:
            x = xxhash.xxh64()
            x.update(key)
            return self.connection.get("content:"+x.hexdigest())


class Website:
    domain = ""
    site_id = 0
    robots_content = ""
    robots_lines = []
    parsed_robots = None
    sitemap_content = ""
    sitemap_url = ""

    def __init__(self, domain=""):
        if len(domain) > 0:
            self.domain = Parser.canonical_url(domain, domain)
            self.get_robots_url()
            self.get_robots_content()
            self.get_sitemap_contents()

    def get_robots_url(self):
        return self.domain.rstrip("/")+"/robots.txt"

    def get_robots_content(self):
        if self.robots_content == "":
            web_driver = Driver(self.get_robots_url())
            self.robots_content = Parser.remove_html_tags(web_driver.web_driver.page_source)
            if not self.check_robots_valid():
                self.robots_content = ""
            else:
                self.robots_lines = self.robots_content.split("\n")

        return self.robots_content

    def get_sitemap_contents(self):
        self.get_robots_content()
        for robot_line in self.robots_lines:
            if re.match("^Sitemap:", robot_line, re.IGNORECASE):
                self.sitemap_url = re.sub("^Sitemap: ", "", robot_line, re.IGNORECASE)
                gcontext = ssl.SSLContext()
                sitemap_content = urlopen(self.sitemap_url, context=gcontext).read()
                self.sitemap_content = sitemap_content.decode("utf-8", errors="ignore")

    def get_sitemap_pages_list(self):
        pages_list = []
        if self.sitemap_content:
            loc_regex = ".+loc$"
            root = xmltree.fromstring(self.sitemap_content)
            for child in root:
                for item in child:
                    if re.match(loc_regex, item.tag, re.IGNORECASE):
                        pages_list.append(item.text)
        return pages_list

    def get_parsed_robots(self):
        if not self.parsed_robots:
            parsed_robots = reppy.robots.Robots.fetch(self.get_robots_url())
            self.parsed_robots = parsed_robots
        return self.parsed_robots

    def check_robots_valid(self):
        if re.match('^User-Agent:', self.robots_content, re.IGNORECASE) is not None:
            return True
        return False

    def save_website(self):
        database = Database()
        database.connect()
        print("Saving website", self.domain)

        r = Redis()
        r.connect()

        if r.get_key("ws"+self.domain) is None:
            r.save_key("ws"+self.domain)
            return database.insert_site(self.domain, self.robots_content, self.sitemap_content)

    def load_website(self, site_id):
        database = Database()
        database.connect(as_dict=True)
        website_data = database.select_website(site_id)
        self.site_id = site_id
        self.domain = website_data["domain"]
        self.sitemap_content = website_data["sitemap_content"]
        self.robots_content = website_data["robots_content"]

    def validate_domain(self, page_url):
        parsed_domain = urlparse(self.domain)
        parsed_page_url = urlparse(page_url)
        return parsed_domain.netloc == parsed_page_url.netloc and "http" in page_url

    def validate_robots_url(self, url):
        try:
            validation_result = self.get_parsed_robots().allowed(url, USER_AGENT)
            if not validation_result:
                print(url, "is not allowed")
            return validation_result
        except:
            return True

    def get_robots_crawl_delay(self):
        agent = self.get_parsed_robots().agent(USER_AGENT)
        delay = agent.delay
        if delay is None:
            return 0
        else:
            return delay

    @staticmethod
    def website_exists(url):
        database = Database()
        database.connect()
        if database.select_website_by_url(url) is None:
            return False
        return True


class Page:
    url = ""
    site_id = ""
    level = 0
    html_content = ""
    http_status_code = 200
    page_type_code = "HTML"
    page_id = 0

    def __init__(self, url, site_id, domain, level=0):
        self.url = Parser.canonical_url(url, domain)
        self.site_id = site_id
        self.level = level

    def queue_page(self, parent_id=0, routing_key="sites"):
        rabbit_mq = RabbitMq().connect()
        data = {
            "site_id": self.site_id,
            "url": self.url,
            "level": self.level,
            "parent_id": parent_id
        }
        json_string = json.dumps(data)

        r = Redis()
        r.connect()
        redis_value = r.get_key(self.url+routing_key)

        if redis_value is None:
            print("Queueing page", self.url)
            rabbit_mq.enqueue(routing_key, json_string)
            r.save_key(self.url+routing_key)
        else:
            print("Skipping url")

    def load_page(self, url, level):
        database = Database()
        database.connect(as_dict=True)
        page = database.select_page(url)
        self.url = url
        self.site_id = page["site_id"]
        self.level = level
        self.html_content = page["html_content"]
        self.http_status_code = page["http_status_code"]
        self.page_type_code = page["page_type_code"]
        self.page_id = page["id"]

    def save_page(self):
        database = Database()
        database.connect()
        page_id = database.insert_page(self.site_id, self.page_type_code, self.url, self.html_content, self.http_status_code, datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S'))

        r = Redis()
        r.connect()
        r.save_content_key(self.html_content, page_id)

        return page_id

    def update_page(self):
        database = Database()
        database.connect()
        database.update_page(self.url, self.site_id, self.page_type_code, self.html_content, self.http_status_code, datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S'))


class Initializer:
    file = None

    def __init__(self, file):
        self.file = file

    def crawl_seed_pages(self):
        urls = UrlFetcher.getInitialUrls(self.file)

        for url in urls:
            ws = Website(url)
            site_id = ws.save_website()

            for sitemap_page_url in ws.get_sitemap_pages_list():
                sm_pg = Page(sitemap_page_url, site_id, ws.domain, level=1)
                sm_pg.queue_page()

            pg = Page(url, site_id, ws.domain)
            pg.queue_page()


class Consumer:
    identificator = ""

    def __init__(self):
        self.identificator = uuid.uuid4().hex[:6].upper()

    def process_site_selenium(self, ch, method, properties, body):
        try:
            data = json.loads(body.decode("utf-8", errors="ignore"))
            current_page_url = data["url"]
            site_id = data["site_id"]
            level = data["level"]

            database = Database()
            database.connect()

            website = Website()
            website.load_website(site_id)

            # Respect crawl delay!
            time.sleep(website.get_robots_crawl_delay())

            selenium_page = Page(current_page_url, site_id, website.domain, level=level+1)
            selenium_page.load_page(current_page_url, level+1)

            web_driver = Driver(current_page_url)
            selenium_page.html_content = web_driver.web_driver.page_source
            selenium_page.update_page()

            for link in Parser.get_page_links(web_driver.web_driver):
                page_url = link.get_attribute("href")
                page_url = Parser.canonical_url(page_url, website.domain)
                if Parser.validate_url(page_url) and website.validate_domain(page_url) and website.validate_robots_url(page_url) and level <= MAX_LEVEL:
                    pg = Page(page_url, site_id, website.domain, level=level+1)
                    pg.queue_page(selenium_page.page_id)
                if (not website.validate_domain(page_url)) and self.check_gov_domain(page_url):
                    print("New website discovered selenium:", page_url)
                    self.process_website(page_url, selenium_page.page_id)

            for image in Parser.get_page_images(web_driver.web_driver):
                image_url = image.get_attribute('src')
                if image_url is not None and "data:image" not in image_url:
                    database.insert_image(selenium_page.page_id, image_url[image_url.rfind("/")+1:], image_url[image_url.rfind(".")+1:], '')

        except psycopg2.IntegrityError:
            print("Database Integrity Error occured in Consumer:", self.identificator, "with url:", body.decode("utf-8", errors="ignore"))

        print("Sending ack")
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def process_site(self, ch, method, properties, body):
        try:
            database = Database()
            database.connect()
            data = json.loads(body.decode("utf-8", errors="ignore"))
            current_page_url = data["url"]
            site_id = data["site_id"]
            level = data["level"]
            source_page_id = data["parent_id"]

            website = Website()
            website.load_website(site_id)

            # Respect crawl delay!
            time.sleep(website.get_robots_crawl_delay())

            current_page = Page(current_page_url, site_id, website.domain, level+1)
            gcontext = ssl.SSLContext()
            opened_page = urlopen(current_page_url, context=gcontext)
            # HANDLE DOCUMENTS
            content_type = opened_page.info().get_content_subtype()
            if content_type == 'pdf' or content_type == 'doc' or content_type == 'docx' or content_type == 'ppt' or content_type == 'pptx':
                current_page.html_content = None
                current_page.page_type_code = "BINARY"
                current_page.http_status_code = opened_page.getcode()
                page_id = current_page.save_page()

                database.insert_page_data(page_id, content_type.upper(), "")
                database.insert_page_link(source_page_id, page_id)

            elif content_type == 'html':
                current_page.html_content = opened_page.read().decode("utf-8", errors="ignore")
                current_page.page_type_code = "HTML"
                current_page.http_status_code = opened_page.getcode()

                r = Redis()
                r.connect()
                duplicate_page_id = r.get_content_key(current_page.html_content)
                if duplicate_page_id is not None and len(current_page.html_content) >= UNRENDERED_PAGE_CHARACTER_LIMITATION:
                    current_page.page_type_code = "DUPLICATE"
                    current_page.html_content = ""
                    page_id = current_page.save_page()
                    # Create link
                    database.insert_page_link(page_id, duplicate_page_id.decode("utf-8"))
                else:
                    page_id = current_page.save_page()
                    database.insert_page_link(source_page_id, page_id)

                    # HANDLE SELENIUM QUEUING
                    if len(current_page.html_content) < UNRENDERED_PAGE_CHARACTER_LIMITATION:
                        current_page.queue_page(page_id, "selenium_sites")
                    else:
                        soup = BeautifulSoup(current_page.html_content)
                        for link in soup.findAll('a'):
                            page_url = link.get("href")
                            if page_url is not None:
                                page_url = Parser.canonical_url(page_url, website.domain)
                                if Parser.validate_url(page_url) and website.validate_domain(page_url) and website.validate_robots_url(page_url) and level <= MAX_LEVEL:
                                    pg = Page(page_url, site_id, website.domain, level=level+1)
                                    pg.queue_page(page_id)
                                if (not website.validate_domain(page_url)) and self.check_gov_domain(page_url):
                                    print("New website discovered normal:", page_url)
                                    self.process_website(page_url, page_id)
                        for image in soup.findAll('img'):
                            image_url = image.get('src')
                            print("Processing img:", image_url)
                            if image_url is not None and "data:image" not in image_url:
                                database.insert_image(page_id, image_url[image_url.rfind("/")+1:], image_url[image_url.rfind(".")+1:], '')

            else:
                print("Invalid content type")

        except psycopg2.IntegrityError as e:
            print("Database Integrity Error occured in Consumer:", self.identificator, "with url:", body.decode("utf-8", errors="ignore"), "message:", e.pgerror)
        except UnicodeEncodeError:
            print("Encoding error")
        except HTTPError as e:
            # HANDLE 404!
            pg = Page(current_page_url, site_id, website.domain, level=level+1)
            pg.http_status_code = e.code
            try:
                page_id = pg.save_page()
                database.insert_page_link(source_page_id, page_id)
            except psycopg2.IntegrityError:
                print("Database Integrity Error occured in Consumer:", self.identificator, "with url:", body.decode("utf-8", errors="ignore"))

        print("Sending ack")
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def process_website(self, url, source_page_id):
        if not ALLOW_EXTERNAL_SITES or "mailto" in url:
            return None

        url = Parser.get_domain(url)
        print("Website exists:", Website.website_exists(url), url)
        if Website.website_exists(url):
            return None

        website = Website(Parser.get_domain(url))
        site_id = website.save_website()

        print("Website saved:", url)

        for sitemap_page_url in website.get_sitemap_pages_list():
            sm_pg = Page(sitemap_page_url, site_id, website.domain, level=1)
            sm_pg.queue_page(source_page_id)

        if Parser.validate_url(url) and website.validate_robots_url(url):
            pg = Page(url, site_id, website.domain, level=0)
            pg.queue_page(source_page_id)

    def check_gov_domain(self, page_url):
        return '.gov.si' in page_url

    def start_consumer(self):
        callback = self.process_site
        connection = pika.BlockingConnection(pika.ConnectionParameters('10.1.0.3'))
        channel = connection.channel()
        channel.basic_consume(callback, queue='sites')
        channel.basic_qos(prefetch_count=10)
        channel.start_consuming()

    def start_selenium_consumer(self):
        callback = self.process_site_selenium
        connection = pika.BlockingConnection(pika.ConnectionParameters('10.1.0.3'))
        channel = connection.channel()
        channel.basic_consume(callback, queue='selenium_sites')
        channel.basic_qos(prefetch_count=10)
        channel.start_consuming()


if sys.argv[1] == 'initialize':
    initializer = Initializer("gov-sites.txt")
    rabbit_mq = RabbitMq()
    rabbit_mq.connect()
    rabbit_mq.declare_queue("sites")
    rabbit_mq.declare_queue("selenium_sites")
    initializer.crawl_seed_pages()

if sys.argv[1] == 'crawl':
    consumer = Consumer()
    consumer.start_consumer()

if sys.argv[1] == 'crawl_selenium':
    consumer = Consumer()
    consumer.start_selenium_consumer()

if sys.argv[1] == 'test':
    print(Parser.get_domain("http://www.e-prostor.gov.si/", True))
