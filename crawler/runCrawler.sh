#!/bin/bash

for (( c=1; c<=$1; c++ ))
do
	docker run --network=docker_vpcbr --rm --name my-running-script-$c -v "$PWD":/usr/src/myapp -w /usr/src/myapp markoambrozic/python:latest python crawler.py crawl 2>&1 >> crawl.log &
    docker run --network=docker_vpcbr --rm --name my-running-script-selenium-$c -v "$PWD":/usr/src/myapp -w /usr/src/myapp markoambrozic/python:latest python crawler.py crawl_selenium 2>&1 >> crawl_selenium.log &
done
