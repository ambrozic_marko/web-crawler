#!/bin/bash
docker run -it --network=docker_vpcbr --rm --name my-running-script-$1 -v "$PWD":/usr/src/myapp -w /usr/src/myapp markoambrozic/python:latest python crawler.py initialize
