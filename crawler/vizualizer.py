import plotly

import plotly.plotly as py
import plotly.graph_objs as go
import networkx as nx
from plotly.offline import download_plotlyjs, init_notebook_mode,  iplot, plot
import psycopg2
import psycopg2.extras


class Database:
    connection = None
    cursor = None
    user = "root"
    password = "root"
    host = "10.1.0.2"
    database = "postgres"

    def connect(self, as_dict = False):
        self.connection = psycopg2.connect(
            user=self.user,
            password=self.password,
            host=self.host,
            database=self.database
        )

        if as_dict:
            self.cursor = self.connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
        else:
            self.cursor = self.connection.cursor()

    def select_all_page_ids(self, url):
        self.cursor.execute(
            """SELECT id, url FROM crawldb.page WHERE url LIKE CONCAT('%%', %s, '%%')"""
            , (url,)
        )
        return self.cursor.fetchall()

    def select_all_page_links(self, page_ids):
        self.cursor.execute(
            """SELECT * FROM crawldb.link WHERE from_page IN %s OR to_page IN %s""",
            (page_ids, page_ids, )
        )
        return self.cursor.fetchall()


db = Database()
db.connect()
page_ids = []
labels = {}

for id_tuple in db.select_all_page_ids("sova.gov.si"):
    page_ids.append(id_tuple[0])
    labels[id_tuple[0]] = id_tuple[1]
page_links = db.select_all_page_links(tuple(page_ids))

init_notebook_mode(connected=True)

plotly.tools.set_credentials_file(username='ma6795', api_key='5BF1mc7AGvLSJeIDjgXi')

G = nx.Graph()  # G is an empty Graph
G.add_nodes_from(page_ids)
my_edges = page_links
G.add_edges_from(my_edges)

pos = nx.fruchterman_reingold_layout(G)

Xn = [pos[k][0] for k in pos]
Yn = [pos[k][1] for k in pos]

node_labels = [labels[id] for id in page_ids]

trace_nodes = dict(type='scatter',
                   x=Xn,
                   y=Yn,
                   mode='markers',
                   marker=dict(size=14, color='rgb(0,240,0)'),
                   text=node_labels,
                   )

Xe = []
Ye = []
for e in G.edges():
    Xe.extend([pos[e[0]][0], pos[e[1]][0], None])
    Ye.extend([pos[e[0]][1], pos[e[1]][1], None])

trace_edges = dict(type='scatter',
                   mode='lines',
                   x=Xe,
                   y=Ye,
                   line=dict(width=1, color='rgb(25,25,25)'),
                   hoverinfo='none'
                   )

axis = dict(showline=False,  # hide axis line, grid, ticklabels and  title
            zeroline=False,
            showgrid=False,
            showticklabels=False,
            title=''
            )

layout = dict(title='sova.gov.si',
              font=dict(family='Balto'),
              width=1920,
              height=1080,
              autosize=False,
              showlegend=False,
              xaxis=axis,
              yaxis=axis,
              margin=dict(
                  l=40,
                  r=40,
                  b=85,
                  t=100,
                  pad=0,

              ),
              hovermode='closest',
              plot_bgcolor='#efecea',  # set background color
              )

fig = dict(data=[trace_edges, trace_nodes], layout=layout)

py.iplot(fig, filename='networkx')



